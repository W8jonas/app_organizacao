from tkinter import *
import time
import datetime

root = Tk()
root.title("Sis")
root.resizable(False, False)
root.configure(background='gray')

frame__ = Frame(root, bg="blue", bd=10, relief=RIDGE)
frame__.grid()


def plot_cabecalho():
    Date1 = StringVar()
    Hora1 = StringVar()
    Total_atv = StringVar()

    Date1.set("Data: " + time.strftime("%d/%m/%Y"))
    Hora1.set("Horário: " + time.strftime("%H:%M:%S"))

    Total_atv.set("Total Atividades: " + len(vetor_de_atividades).__str__())
    frame_data_e_hora = Frame(frame__, bg="green", bd=10, relief=RIDGE)
    frame_data_e_hora.grid(row=0, column=0, columnspan=4, sticky=W)

    Label(frame_data_e_hora, textvariable=Date1, font=('arial', int(tamanho_fonte * 1.5), 'bold'), padx=8, pady=1,
          width=15, bd=1, bg="green", fg="black", justify=CENTER).grid(row=0, column=0)

    Label(frame_data_e_hora, textvariable=Total_atv, font=('arial', int(tamanho_fonte * 1.5), 'bold'), padx=5, pady=5,
          width=15, bd=1, bg="green", fg="black", justify=CENTER).grid(row=0, column=1)

    Label(frame_data_e_hora, textvariable=Hora1, font=('arial', int(tamanho_fonte * 1.5), 'bold'), padx=5, pady=5,
          width=15, bd=1, bg="green", fg="black", justify=CENTER).grid(row=0, column=2)


def plot_vetor_de_frames():
    cont_colunas = 0

    for i in range(0, len(vetor_de_atividades)):  # Cria os frames de cada atividade
        frame_fluido = Frame(frame__, bg=Cor_de_fundo_Vetor_de_frames, bd=5, relief=RAISED)
        vetor_de_frames.append(frame_fluido)

    cont_axu = 1
    cont_axu_row = 1
    for i in range(0, len(vetor_de_atividades)):  # Imprimir texto "Atividade 1,2,3 ..."
        global Qtd_ativ_por_coluna

        if i > Qtd_ativ_por_coluna:
            cont_colunas += 1
            Qtd_ativ_por_coluna = Qtd_ativ_por_coluna + Qtd_ativ_por_coluna_fixo
            cont_axu_row = 1
            print(f'i {i}')
            print(f'qtd {Qtd_ativ_por_coluna}')
            print(f'con {cont_colunas}')
            print("______________________")

        Cont_atividades = StringVar()
        Cont_atividades.set("Atividade: " + cont_axu.__str__())
        Label(vetor_de_frames[i], textvariable=Cont_atividades, font=('arial', int(tamanho_fonte), 'normal'), padx=2,
              pady=0, bd=2, bg=Cor_de_fundo_Vetor_de_frames, fg="black", justify=LEFT).grid(row=cont_axu, column=0)
        vetor_de_frames[i].grid(row=cont_axu_row, column=cont_colunas, columnspan=10, sticky=W)
        cont_axu += 1
        cont_axu_row += 1

    cont_axu = 0
    for i in vetor_de_atividades:  # Imprimir a atividade em cada frame
        dado_fluido = StringVar()
        dado_fluido.set(i)
        frame_fluido = vetor_de_frames[cont_axu]
        tam = len(dado_fluido.get())
        Label(frame_fluido, textvariable=dado_fluido, font=('arial', int(tamanho_fonte*1.2), 'bold'), padx=5, pady=5,
              width=21, bd=9, bg=Cor_de_fundo_Vetor_de_frames, fg="black", justify=CENTER).grid(row=cont_axu + 2,
                                                                                                column=1)
        cont_axu += 1
    return cont_axu


def plot_tela_1():

    plot_cabecalho()

    contador_de_linha = plot_vetor_de_frames()
    contador_de_linha += 1

    botao_nova_tarefa = Button(frame__, padx=1, pady=1, bd=5, fg="black", font=('arial', int(tamanho_fonte * 1.4), 'bold'),
                               width=10, bg="green", text="Nova tarefa").grid(row=contador_de_linha, column=0)

    botao_livros = Button(frame__, padx=1, pady=1, bd=5, fg="black", font=('arial', int(tamanho_fonte * 1.4), 'bold'),
                          width=10, bg="orange", text="Livros").grid(row=contador_de_linha, column=1)

    botao_sair = Button(frame__, padx=1, pady=1, bd=5, fg="black", font=('arial', int(tamanho_fonte * 1.4), 'bold'),
                        width=10, bg="red", text="sair").grid(row=contador_de_linha, column=2)

    root.mainloop()


tamanho_fonte = 10

Cor_de_fundo_Vetor_de_frames = "purple"
vetor_de_atividades = ["Atividade_X01",
                       "Atividade_X02",
                       "Atividade_X03",
                       "Atividade_X04",
                       "Atividade_X05",
                       "Atividade_X06",
                       "Atividade_X07",
                       "Atividade_X08",
                       "Atividade_X09",
                       "Atividade_X10",
                       "Atividade_X11",
                       "Atividade_X12",
                       "Atividade_X13",
                       "Atividade_X14",
                       "Atividade_X15",
                       "Atividade_X16",
                       "Atividade_X17",
                       "Atividade_X18",
                       "Atividade_X19",
                       "Atividade_X20",
                       "Atividade_X21",
                       "Ultima_Atividade"]

vetor_de_frames = []

Qtd_ativ_por_coluna = 3
Qtd_ativ_por_coluna_fixo = Qtd_ativ_por_coluna + 1
plot_tela_1()
